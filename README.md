# Tools

#### gpio_*.py

Permite ligar, desligar e obter o status de um canal GPIO

./gpio_asus.py - Pré-configurado para a placa Tinkerboard.

./gpio_board.py - Pré-configurado para todas as placas (compativeis com GPIO.BOARD).



## Installation

sudo git clone https://gitlab.com/winston-iot/tinkerboard-tools.git

cd tinkerboard-tools

## Usage

### GPIO_*.py
```shell
./gpio_*.py {action} {target port}
#example:
./gpio_*.py -l 17 #Turn On: 
./gpio_*.py -d 17 #Turn Off: 
./gpio_*.py -s 17 #Get Status: 
```
### lcd_oled.py
```shell
./lcd_oled.py {"line0"} {"line1"} {"line2"} {"line3"}
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
