#!/bin/sh
sudo apt-get update
sudo apt-get install python-dev
git clone http://github.com/TinkerBoard/gpio_lib_python --depth 1 GPIO_API_for_Python
cd GPIO_API_for_Python/
sudo python setup.py install