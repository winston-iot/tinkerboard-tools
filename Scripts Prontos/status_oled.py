#!/usr/bin/env python
# -*- coding: utf-8 -*-
import Adafruit_SSD1306
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

#Aditional for scripts and functions
import subprocess
import time
import sys
# Raspberry Pi pin configuration:
RST = None     # on the PiOLED this pin isnt used
# 128x32 display with hardware I2C:
disp = Adafruit_SSD1306.SSD1306_128_32(rst=RST)
# Initialize library.
disp.begin()
# Clear display.
disp.clear()
disp.display()
# Create blank image for drawing.
# Make sure to create image with mode '1' for 1-bit color.
width = disp.width
height = disp.height
image = Image.new('1', (width, height))
# Get drawing object to draw on image.
draw = ImageDraw.Draw(image)
# Draw a black filled box to clear the image.
draw.rectangle((0,0,width,height), outline=0, fill=0)
# Draw some shapes.
# First define some constants to allow easy resizing of shapes.
padding = -2
top = padding
bottom = height-padding
# Move left to right keeping track of the current x position for drawing shapes.
x = 0
# Load default font.
font = ImageFont.load_default()
# Alternatively load a TTF font.  Make sure the .ttf font file is in the same directory as the python script!
# Some other nice fonts to try: http://www.dafont.com/bitmap.php
# font = ImageFont.truetype('Minecraftia.ttf', 8)
legendline0 = "Lan:"
legendline1 = "Wan:"
legendline2 = "line 3"
legendline3 = "line 4"

line1 = ""

count = 109
countLimit = 120
while True:
    # Draw a black filled box to clear the image.
    draw.rectangle((0,0,width,height), outline=0, fill=0)
    count += 1
    ##Bloco de comandos de informação
    cmd = "hostname -I | cut -d\' \' -f1"
    line0 = str(subprocess.check_output(cmd, shell = True ))
    
    if count >= 110 and count < countLimit:
        line1 = "Updating"
    elif count >= countLimit:    
        try:
            cmd = "curl ifconfig.me"
            line1 = str(subprocess.check_output(cmd, shell = True ))
        except:
            line1 = "No Connection"
        count = 0

    cmd = ""
    line2 = str(subprocess.check_output(cmd, shell = True ))
    cmd = ""
    line3 = str(subprocess.check_output(cmd, shell = True ))
    ##Fim do bloco de comandos
    # Write two lines of text.
    draw.text((x, top), legendline0 + str(line0),  font=font, fill=255)
    draw.text((x, top+8), legendline1 + str(line1),  font=font, fill=255)
    draw.text((x, top+16), legendline2 + str(line2),  font=font, fill=255)
    draw.text((x, top+25), legendline3 + str(line3 + " " + str(count)),  font=font, fill=255)
    # Display image.
    disp.image(image)
    disp.display()
    #run every n seconds
    time.sleep(1)