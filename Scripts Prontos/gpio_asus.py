#!/usr/bin/env python
# -*- coding: utf-8 -*-
import ASUS.GPIO as GPIO
import time
import sys

GPIO.setwarnings(False)
GPIO.setmode(GPIO.ASUS)

#usage: python on_off_gpio.py {action} {target port}
#example:
#  Turn On: python on_off_gpio.py -l 17
#  Turn Off: python on_off_gpio.py -d 17
#  Get Status: python on_off_gpio.py -s 17

def poweron(param):
    port = param
    GPIO.setup(port,GPIO.OUT)
    GPIO.output(port,GPIO.HIGH)


def poweroff(param):
    port = param
    GPIO.setup(port,GPIO.OUT)
    GPIO.output(port,GPIO.LOW)


def getstatus(param):
    port = param
    GPIO.setup(port,GPIO.OUT)
    status = GPIO.input(port)
    return status


if sys.argv[1] == '-l':
    print ('Turn On')
    poweron(int(sys.argv[2]))
elif sys.argv[1] == '-d':
    print ('Turn Off')
    poweroff(int(sys.argv[2]))
elif sys.argv[1] == '-s':
    print ('Status')
    print getstatus(int(sys.argv[2]))    

    