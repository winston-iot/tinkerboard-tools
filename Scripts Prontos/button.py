#!/usr/bin/env python
# -*- coding: utf-8 -*-
import ASUS.GPIO as GPIO
import time
import sys
import subprocess

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)

result = 0

GPIO.setup(12, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
# Set pin 10 to be an input pin and set initial value to be pulled low (off)
while True: # Run forever
    if GPIO.input(13) == GPIO.HIGH:
        result += 1
        print(result)
        time.sleep(2)
        cmd = "sudo service network-manager restart"
        command = subprocess.check_output(cmd, shell = True )
        print("Restarting Network")
        print(str(command))