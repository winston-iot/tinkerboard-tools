#!/usr/bin/env python
# -*- coding: utf-8 -*-
import ASUS.GPIO as GPIO
import time
import sys

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)

#usage: python on_off_gpio.py {action} {target port}
#example:
#  Turn On: python on_off_gpio.py -l 17
#  Turn Off: python on_off_gpio.py -d 17
#  Get Status: python on_off_gpio.py -s 17

def poweron(param):
    port = param
    GPIO.setup(port,GPIO.OUT)
    GPIO.output(port,GPIO.HIGH)


def poweroff(param):
    port = param
    GPIO.setup(port,GPIO.OUT)
    GPIO.output(port,GPIO.LOW)


def getstatus(param):
    port = param
    GPIO.setup(port,GPIO.OUT)
    status = GPIO.input(port)
    return status

duration = 10
dat = 0
interval = 0.1
port = 11

while (dat <= duration):
    poweron(port)
    time.sleep(interval)
    poweroff(port)
    dat += 1
